* xref:index.adoc[]

* GraphQL API

** xref:graphql-examples.adoc[]

* REST API

** xref:rest-examples.adoc[]
